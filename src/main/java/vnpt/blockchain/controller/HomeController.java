package vnpt.blockchain.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vnpt.blockchain.model.Block;
import vnpt.blockchain.model.BlockChain;
import vnpt.blockchain.model.Transaction;

@Controller
@RequestMapping("")
public class HomeController
{
    BlockChain blockChain= new BlockChain();
    @RequestMapping("/index")
    public String index()
    {
        return "index";
    }
    @RequestMapping("/mine")
    public @ResponseBody Block mine()
    {
        Block lastBlock = blockChain.lastBlock();
        int lastProof = lastBlock.proof;
        int proof = blockChain.proofOfWork(lastProof);
        //blockChain.newTransaction("God", "Nhi", 1);
        String previousHash=blockChain.hash(lastBlock);
        Block block = blockChain.newBlock(proof, previousHash);

        return block;
    }
    @RequestMapping("/transactions/new")
    public @ResponseBody String newTransaction(String sender, String recipient, Integer amount)
    {
        Transaction transaction= new Transaction(sender, recipient, amount);
        blockChain.currentTransactions.add(transaction);
        return "Added new transaction";
    }
    @RequestMapping("/fullChain")
    public @ResponseBody List<Block> fullChain()
    {
        return blockChain.chain;
    }
    @RequestMapping("/nodes/register")
    public @ResponseBody ArrayList<String> registerNodes(String[] nodes)
    {
        for (String node : nodes) {
            blockChain.registerNode(node);
        }
        return blockChain.nodes;
    }
    @RequestMapping("/nodes/resolve")
    public @ResponseBody String consensus(String[] nodes)
    {
        if(blockChain.resolveConflicts())
        {
            return "Shit! We fucked up!";
        }
        else
        {
            return "We good! Someone fucked up.";
        }
    }
    @RequestMapping("/selfvalid")
    public @ResponseBody String selfvalid()
    { 
        
        return blockChain.validChain(blockChain.chain)+"";
    }
    @RequestMapping("/hash")
    public @ResponseBody String hash(int index) 
    {
        return blockChain.hash(blockChain.chain.get(index));
    }
}