package vnpt.blockchain.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Block
{
    public Integer index;
    public Timestamp timeStamp;
    public ArrayList<Transaction> transactions;
    public int proof;
    public String previousHash;
}