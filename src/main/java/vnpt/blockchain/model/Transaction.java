package vnpt.blockchain.model;

public class Transaction {
    public String sender;
    public String recipient;
    public Integer amount;
    public Transaction(){}
    public Transaction(String sender, String recipient, Integer amount) {
        this.amount=amount;
        this.recipient=recipient;
        this.sender=sender;
    }
}