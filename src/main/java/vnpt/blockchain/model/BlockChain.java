package vnpt.blockchain.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class BlockChain {
    public ArrayList<Block> chain;
    public ArrayList<Transaction> currentTransactions;
    public ArrayList<String> nodes;
    CloseableHttpClient client = HttpClients.createDefault();
    ObjectMapper mapper = new ObjectMapper();

    public BlockChain() {
        this.chain = new ArrayList<Block>();
        this.currentTransactions = new ArrayList<Transaction>();
        this.nodes = new ArrayList<String>();
        // Tạo genesis block
        newBlock(100, "1");
    }

    public Block newBlock(int proof, String previousHash) {
        previousHash = previousHash == null ? "" : previousHash;
        Block genesisBlock = new Block();
        genesisBlock.index = this.chain.size() + 1;
        genesisBlock.timeStamp = new Timestamp(new Date().getTime());
        genesisBlock.transactions = this.currentTransactions;
        genesisBlock.proof = proof;
        genesisBlock.previousHash = previousHash != null ? previousHash : this.hash(this.lastBlock());

        this.currentTransactions = new ArrayList<Transaction>();
        this.chain.add(genesisBlock);
        return genesisBlock;
    }

    public int newTransaction(String sender, String recipient, Integer amount) {
        Transaction trans = new Transaction(sender, recipient, amount);
        this.currentTransactions.add(trans);
        return lastBlock().index + 1;
    }

    public static String hash(Block block) {
        ObjectMapper _mapper = new ObjectMapper();
        String json="";
        try {
             json = _mapper.writeValueAsString(block);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String sha256hex = Hashing.sha256().hashString(json, StandardCharsets.UTF_8).toString();
        return sha256hex;
    }

    public Block lastBlock() {
        return chain.get(chain.size() - 1);
    }

    public int proofOfWork(int lastProof) {
        int proof = 0;
        while (!isProofValid(lastProof, proof))
            proof++;
        return proof;
    }

    public boolean isProofValid(int lastProof, int proof) {
        return (proof-lastProof==100);
    }

    public void registerNode(String address) {
        if (!this.nodes.contains(address)) {
            this.nodes.add(address);
        }
    }

    public boolean validChain(ArrayList<Block> chain) {
        Block lastBlock = chain.get(0);
        int currentIndex = 1;
        while (currentIndex < chain.size()) {
            Block block = chain.get(currentIndex);

            // Kiem tra xem hash cua block co dung khong
            String hashString = this.hash(lastBlock);
            if (!block.previousHash.equals(hashString))
                return false;

            // Kiem tra xem proof of work co dung khong
            if (!this.isProofValid(lastBlock.proof, block.proof))
                return false;
            currentIndex++;
            lastBlock=block;
        }
        return true;
    }

    public boolean resolveConflicts() {

        ArrayList<String> neighbours = this.nodes;
        ArrayList<Block> newChain = new ArrayList<>();
        int maxLength = this.chain.size();
        for (String node : neighbours) {
            String data = this.post(node);
            try {
                ArrayList<Block> chain = mapper.readValue(data, new TypeReference<ArrayList<Block>>() {
                });
                int length = chain.size();

                if (length > maxLength && this.validChain(chain)) {
                    maxLength = length;
                    newChain = chain;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (newChain.size() > 0) {
            this.chain = newChain;
            return true;
        }
        return false;
    }

    public String post(String node) {
        String requestURL = node + "/fullChain";

        try {
            URL url = new URL("http://" + requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(false);
            System.out.println(requestURL + ": " + conn.getResponseCode() + " --> Type: " + conn.getContentType());
            InputStream inputStream = conn.getInputStream();
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
            String data = writer.toString();
            conn.disconnect();
            return data;

        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

}